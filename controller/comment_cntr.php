<?php
require_once '../config/classload.php';

$comment = new Comment();

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'add' )
{
     if (!isset($_SESSION['user_id']))
    {
        echo "Please login to add a new link here";
        return;
    }
    $stat = $comment->add($_REQUEST);
    
    if ($stat == TRUE)
    {
        echo "Added Comment Successfully";
        $_SESSION['suc_msg'] = "Added the comment successfully";
        
        //header('Location:dashboard');
    }
    else
    {
        echo "Unable to comment";
        $_SESSION['suc_msg'] = "Unable to comment";
        //header('Location:dashboard');
    }
}

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'getall' )
{
    $data = $activitie->getAll();
    $data = array('aaData'=>$data);
    echo json_encode($data);
}

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete' )
{
    
    $stat = $activitie->delet($_REQUEST['id']);
    if ($stat == TRUE)
    {
        $_SESSION['suc_msg'] = "Delete the user successfully";
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
    else
    {
        $_SESSION['suc_msg'] = "Unable to delete the user";
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
}

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'update' )
{
    $stat = $activitie->update($_REQUEST,$_FILES);
    if ($stat == TRUE)
    {
        $_SESSION['suc_msg'] = "Updated the user successfully";
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
    else
    {
        $_SESSION['suc_msg'] = "Unable to update the user";
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
}

?>