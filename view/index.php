<?php
require_once '../config/config.php';
require_once '../config/classload.php';
//die(print_r($_SESSION));
$postObj = new Photo();
$commentObj = new Comment();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title><?php echo PROJECT_NAME; ?></title>
    
<?php require_once 'includes/header.php'; ?>

</head>

<body style="background: white;" >

   
         
    <?php require_once 'includes/navbar.php'; ?>
     
<div style = 'height:80%;' >	 
    <div class="search-box"> 

        <form method="post" id="searchform" >
            <input name="searchurl" id="idsearchurl" value="" type="text" class="form-control" placeholder = "Enter URL here and search" />
		<i class="glyphicon glyphicon-search"></i>
        </form>
	</div>
	
	<br>
	<div>
		<input id="idaddlinkbtn" type="button" class="btn btn-success" value = "+" />
                <?php if (isset($_REQUEST['searchurl']) && $_REQUEST['searchurl'] != '' ) { 
                    $postExists = $postObj->postExists($_REQUEST['searchurl']);
                    $postDet =  $postObj->getPostDetails($_REQUEST['searchurl']);
                    if ($postExists>=1)
                    {
                    ?>
                <a style="margin-left:5%;" href="<?php echo $_REQUEST['searchurl']; ?>" target="_blank" >Link</a> <input style="margin-left:5%;margin-top: 1%;" type="image" id="idcomment" src="images/comment.png" width="25px" height="25px" />
                    <?php }
                    else
                    { ?>
                <h4>Link does not exists. Please add this link first to this site.</h4>
                   <?php     
                    }
                    } ?>
	</div>
        <div class="container" style="height: 80%;overflow-y: auto;" >
            <div class="row">
                <div class="col-md-8">
                  <div class="page-header">
                      <h1><small class="pull-right"><?php echo $commentObj->getTotalComments($postDet['serial_no']); ?> comments</small> Comments </h1>
                  </div> 
                    <?php $allComments = $commentObj->getAllByPostId($postDet['serial_no']);
                    foreach($allComments as $comment)
                    {
                        //die(print_r($comment));
                    ?>
                    <div style="margin-top:10px;"  class="comments-list">
                       <div class="media">
                           <p class="pull-right"><small><?php echo $comment['date_time']; ?></small></p>
                            <a class="media-left" href="#">
                                <img src="<?php echo PROFILE_PICS_FOLDER.'/'.$comment['profile_image']; ?>"  width="50px" height="50px" />
                            </a>
                            <div class="media-body">
                                
                              <h4 class="media-heading user_name"><?php echo $comment['first_name']; ?> <?php echo $comment['last_name']; ?></h4>
                              <?php echo $comment['comment']; ?>
                              
                              <!--<p><small><a href="">Like</a> - <a href="">Share</a></small></p>-->
                            </div>
                          </div>
                       
                   </div>
                    
                    <?php } ?>
                    
                </div>
            </div>
        </div>
     </div>  
    
    
	<?php require_once 'includes/footer.php' ; ?>
   


   <div id = "idaddlinkdiv" style = "display:none" >
   
    <textarea id = "idnewlink" class = "form-control" ></textarea><br>
	<input id="idpostlinkbtn" type = "button" class="btn btn-success" value = "Add" />
   </div>
    
    <div id = "idaddcomment" style = "display:none" >
   
        <textarea id = "idnewcomment" name="value" class = "form-control" placeholder="Enter Your Comment" ></textarea><br>
        <input type="hidden" value="<?php echo $postDet['serial_no']; ?>" id="idcompostid" />
	<input id="idpostcomment" type = "button" class="btn btn-success" value = "Add" />
   </div>

   
    
</body>

<script>
$(function(){
	
	$("#idaddlinkbtn").on('click',function(){
		
		$("#idaddlinkdiv").dialog({ title:"Add New Link",width:800,height:190 });
		
	});
        
        $("#idcomment").on('click',function(){
            $('#idaddcomment').dialog({ title:"Comment",width:800,height:190 });
        });
	
	$("#idpostlinkbtn").on('click',function(){
            $('#idaddlinkdiv').dialog('close');
		$.ajax({
			url: "photo_cntr",
			method:"post",
			data: { action:"add", value:$("#idnewlink").val() },
			success: function(response){
				alert(response);
                                
			},
			fail: function(response){	
			}
		});
	});
        
        $("#idpostcomment").on('click',function(){
            $('#idaddcomment').dialog('close');
            $.ajax({
                url:"comment_cntr",
                method:"post",
                data: { action:"add",value:$("#idnewcomment").val(),postid:$("#idcompostid").val() },
                success: function(response)
                { 
                    alert(response);
                },
                fail:function(response)
                {
                    
                }
            });
        });
        
       
})

    
</script>

</html>
