-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2016 at 05:22 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `linkcomhere`
--

-- --------------------------------------------------------

--
-- Table structure for table `linkcomhere_comments`
--

CREATE TABLE IF NOT EXISTS `linkcomhere_comments` (
  `serial_no` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `linkcomhere_comments`
--

INSERT INTO `linkcomhere_comments` (`serial_no`, `post_id`, `user_id`, `comment`, `date_time`) VALUES
(7, 0, 1, '', '2016-05-05 00:19:34'),
(8, 0, 1, 'test', '2016-05-05 00:21:30'),
(9, 0, 1, 'test', '2016-05-05 00:21:56'),
(10, 0, 1, 'this is the comment that i am adding for the bing link, please ignore this test here', '2016-05-05 20:59:29'),
(11, 0, 1, 'another one', '2016-05-05 22:29:53'),
(12, 5, 1, 'test', '2016-05-05 22:36:10'),
(13, 5, 1, 'test', '2016-05-05 22:36:22'),
(14, 5, 1, 'Another test comment is here', '2016-05-05 23:03:17'),
(15, 3, 1, 'This is a google comment section here', '2016-05-05 23:05:37'),
(16, 9, 1, 'First Comment', '2016-05-05 23:16:18'),
(17, 9, 1, 'Second Comment', '2016-05-05 23:16:30'),
(18, 9, 1, 'Third comment', '2016-05-05 23:18:05'),
(19, 9, 1, 'Fourth comment', '2016-05-05 23:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `linkcomhere_posts`
--

CREATE TABLE IF NOT EXISTS `linkcomhere_posts` (
  `serial_no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `linkcomhere_posts`
--

INSERT INTO `linkcomhere_posts` (`serial_no`, `user_id`, `content`, `date_time`) VALUES
(1, 1, 'Android is the best platform available in the market', '2016-04-24 21:38:35'),
(2, 1, 'this is a test link that is being added here so please ignore this test here to test the functionality', '2016-05-03 21:45:40'),
(3, 1, 'http://www.google.com', '2016-05-04 21:18:33'),
(4, 1, 'http://www.yahoo.com', '2016-05-04 23:55:21'),
(5, 1, 'http://www.bing.com', '2016-05-04 23:57:36'),
(6, 1, '', '2016-05-05 00:10:03'),
(7, 1, '', '2016-05-05 00:11:08'),
(8, 1, 'justtest.com', '2016-05-05 20:47:19'),
(9, 1, 'testlink.com', '2016-05-05 23:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `linkcomhere_users`
--

CREATE TABLE IF NOT EXISTS `linkcomhere_users` (
  `user_id` int(3) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `gender` int(1) NOT NULL COMMENT '1 means male, 2 means female',
  `phone_no` varchar(16) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `is_admin` int(1) NOT NULL DEFAULT '0',
  `is_super_admin` int(1) NOT NULL DEFAULT '0',
  `act_code` varchar(150) NOT NULL,
  `profile_image` varchar(100) NOT NULL DEFAULT '51.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linkcomhere_users`
--

INSERT INTO `linkcomhere_users` (`user_id`, `first_name`, `last_name`, `user_name`, `gender`, `phone_no`, `user_email`, `user_password`, `is_active`, `is_admin`, `is_super_admin`, `act_code`, `profile_image`) VALUES
(1, 'Sourabh', 'Sourabh', 'last name', 0, '', 'email@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '41.jpg'),
(2, 'Second user', 'Second user last name', '', 0, '', 'email2@email2.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '51.jpg'),
(4, 'first name', 'last name', '', 0, '', 'email4@email4.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '25.jpg'),
(5, 'first name', 'last name', '', 0, '', 'email5@email5.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '25.jpg'),
(6, 'Sourabh', 'Sharma', '', 0, '', 'sourabh@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '66.png'),
(7, 'Achal', 'Sharma', '', 0, '', 'achalsharma@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '67.png'),
(8, 'Superadmin', 'Superadmin', 'Superadmin', 0, '', 'superadmin@superadmin.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 1, 1, '', ''),
(9, 'email', 'email', '', 0, '1010101010', 'email10@email10.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', ''),
(0, 'first name', 'last name', '', 0, '9090909090', 'email@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '20.jpg'),
(0, 'first name', 'last name', '', 0, '', 'email@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 0, 0, '0', '20.jpg'),
(0, 'first name', 'last name', '', 0, '', 'email@email.com', 'aafdc23870ecbcd3d557b6423a8982134e17927e', 1, 0, 0, '0', '20.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
